# Snake

![snake](snake-screenshot.png)

A very simplified implementation of the game Snake in the Godot engine. The project was created in the context of a [tutorial series](https://youtube.com/playlist?list=PLiBLFJ8tU-vUuYNQbfwvug0p-GMAJGmCp) on YouTube.

Features:
- 100% GDScript
- Grid based movement through tilemap
- Shows the number of body parts
